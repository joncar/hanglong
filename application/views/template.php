<!DOCTYPE html>
<html lang="en-us" class="no-js">
    <head>
        <meta charset="utf-8">
        <title><?= empty($title)?'HandLong':$title ?></title>
        <meta name="description" content="The description should optimally be between 150-160 characters.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Madeon08">
        <link rel="shortcut icon" href="<?= base_url() ?>img/favicon.png">        
        <link rel="stylesheet" href="<?= base_url() ?>css/template/style.css" />
        <script src="<?= base_url() ?>js/template/modernizr.custom.js"></script>        
    </head>
    <body>
        <?= $this->load->view($view); ?>        
    </body>   
</html>