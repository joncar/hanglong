<div class="section-area wow">
    <div class="container">
        <div class="row">
            <h1 data-editable data-name="title"></h1>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="section-default border-top" data-editable data-name="content1">
                    
                </div>
            </div>
            <div class="col-xs-4">
                <div class="section-default border-top" data-editable data-name="content2">
                    
                </div>
            </div>
            <div class="col-xs-4">
                <div class="section-default border-top" data-editable data-name="content3">
                    
                </div>
            </div>
        </div>
    </div><!-- /section-area -->
</div>