<style>
    #contentHTML .selected, #contentHTML .selectedClick{
        border:1px solid orange !important;
    }
</style>
<div id='contentHTML'>
    <?= $page ?>
</div>
<?php $this->load->view('cms/tools'); ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function(){
       init();
    });
    
    function init(){
       selected = undefined;
       document.getElementById('contentHTML').contentEditable=true;
       //document.body.contentEditable=true; 
       //document.designMode='on';
       $(function(){
          $("#draggable").draggable();
       });
       $(document).on('mouseover',"span, div, h1,h2,h3,h4,p,label,input",function(e){
           e.stopPropagation();           
           $(this).addClass('selected');
           
       });
       $(document).on('mouseout',"span, div, h1,h2,h3,h4,p,label,input",function(e){
           e.stopPropagation();
           $(this).removeClass('selected');
       });
       $(document).on('click',"#contentHTML span, #contentHTML div,#contentHTML h1, #contentHTML h2,#contentHTML h3,#contentHTML h4,#contentHTML p,#contentHTML label,#contentHTML input",function(e){
           e.stopPropagation();//Show Parameters           
           selected = $(this);
           $("#toolsCSS").show();  
           console.log(selected.css('background'));
           $('#toolsCSS .toolsInput[data-type="color"]').val(selected.css('color'));
           $('#toolsCSS .toolsInput[data-type="background"]').val(selected.css('background'));
           $('#toolsCSS .toolsInput[data-type="background-color"]').val(selected.css('background-color'));
           $('#toolsCSS .toolsInput[data-type="background-image"]').val(selected.css('background-image'));
       });
       
       $(document).on('change','.toolsInput',function(){
           selected.css($(this).data('type'),$(this).val());
       });
    }
    
    function save(){
        data = {data:$("#contentHTML").html()};
        $.post('<?= base_url('paginas/admin/paginas/edit/'.$name) ?>',data,function(data){
            alert('Guardado');
        });
    }
    
    function refresh(){
        document.location.reload();
    }
    
    function bold(){
        if(typeof(selected)!=='undefined'){
            var content = window.getSelection().toString();            
            if(content!=='' && !selected.is('span')){
                var element = '<span style="font-weight:700">'+content+'</span>';
                var repl = selected.html();
                repl = repl.replace(content,element);
                selected.html(repl);
            }
            else{
                console.log(selected.css('font-weight'));
                if(selected.css('font-weight')==='700'){
                    selected.css('font-weight','400');
                    selected.find('span').css('font-weight','400');
                }else{
                    selected.css('font-weight','700');
                }
            }
        }
    }
</script>
    