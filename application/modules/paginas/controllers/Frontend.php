<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }        
        
        /*function read($url){            
            $this->loadView(array('view'=>'read','page'=>$this->load->view($url,array(),TRUE),'title'=>ucfirst(str_replace('-',' ',$url))));
        }*/
        
        function read($url){
            $url = $this->db->get_where('paginas',array('titulo'=>$url));
            if($url->num_rows()>0){
                $url->row()->contenido = str_replace('[formreg]',$this->getFormReg(),$url->row()->contenido);
                $this->loadView(array('view'=>'read','page'=>$url->row(),'title'=>ucfirst(str_replace('-',' ',$url->row()->titulo))));
            }else{
                throw new exception('404','No hem trobat la pagina que sol·licita');
            }
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                $this->loadView(array('view'=>'cms/edit','name'=>$url,'edit'=>TRUE,'page'=>$this->load->view($url,array(),TRUE),'title'=>ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
    }
?>
