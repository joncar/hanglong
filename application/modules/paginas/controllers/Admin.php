<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function paginas($action = '',$id = ''){
            switch($action){
                case 'add':
                    $this->loadView(array('view'=>'cms/add'));
                break;
                case 'insert':
                    $this->form_validation->set_rules('nombre','Nombre','required');
                    $this->form_validation->set_rules('columnas','Columnas','required|integer');
                    if($this->form_validation->run()){
                        file_put_contents('application/modules/paginas/views/'.$_POST['nombre'].'.php',$this->load->view('themes/theme'.$_POST['columnas'],array(),TRUE));
                        header("Location:".base_url('paginas/frontend/editor/'.str_replace('.php','',$_POST['nombre'])));
                        exit;
                    }else{
                        header("Location:".base_url('paginas/admin/paginas/add?msj='.urlencode('Debe llenar los datos faltantes')));
                        exit;
                    }
                break;
                case 'edit':
                    if(!empty($_POST['data']) && !empty($id)){
                        file_put_contents('application/modules/paginas/views/'.$id.'.php',$_POST['data']);
                    }
                break;
                case 'file_upload': 
                    $size = getimagesize($_FILES['image']['tmp_name']);
                    $extension = $_FILES['image']['type'];
                    $extension = explode('/',$extension);
                    $extension = count($extension>1)?$extension[1]:$extension[0];
                    $name = $id.'-'.date("dmHis").'.'.$extension;
                    if(move_uploaded_file($_FILES['image']['tmp_name'],'images/'.$name)){
                        echo json_encode(array('success'=>true,'name'=>$name,'size'=>array($size[0],$size[1])));
                    }else{
                        echo json_encode(array('success'=>false,'name'=>$name));
                    }
                break;
                case 'delete':
                    unlink('application/modules/paginas/views/'.$id);
                    redirect(base_url('paginas/admin/paginas'));
                break;
                default:
                    if(empty($action)){
                        $pages = scandir('application/modules/paginas/views');                
                        $this->loadView(array('files'=>$pages,'view'=>'cms/list'));
                    }
                break;
                    
            }            
        }
        
        function banner(){
            $crud = $this->crud_function('','');            
            $crud->columns('foto');
            if($crud->getParameters()!='list'){
                $crud->field_type('foto','image',array('width'=>'600px','height'=>'300px','path'=>'img/banner'));
            }else{
                $crud->set_field_upload('foto','img/banner');
            }
            $crud->field_type('posicion','dropdown',
                    array(
                        'top:50px; left:0px;'=>'Izquierda - Arriba',
                        'top:160px; left:0px;'=>'Izquierda - Centro',
                        'bottom:0px; left:0px;'=>'Izquierda - Abajo',                        
                        'top:50px; left:30%;'=>'Centro - Arriba',
                        'top:160px; left:30%;'=>'Centro - Centro',
                        'bottom:0px; left:30%;'=>'Centro - Abajo',                        
                        'top:50px; right:0; left:initial;'=>'Derecha - Arriba',
                        'top:160px; right:0; left:initial;'=>'Derecha - Centro',
                        'bottom:0px; right:0; left:initial;'=>'Derecha - Abajo'
                    ));
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
